setwd("~/tmp/MJbreakpoint")

############ Libraries used
library(data.table)
dat= fread("dat2.csv")
dat= na.omit(dat)
poscatch= dat[pds_pue>0,]


#### year filter
  species= unique(poscatch$Espece)
  spyr= vector()
  for (i in species){
    spcatch= poscatch[poscatch$Espece==i,]
    tmp= aggregate(spcatch$Annee,list(spcatch$Annee),length)
    spyr= c(spyr,paste(i,tmp$Group.1[tmp$x>=90])) # this creates a unique species year combo in th filter
  }
  
  dat$specesyr= paste(dat$Espece,dat$Annee) # this creates a unique species year combo in the full data set
  tmp2= dat[dat$specesyr %in% spyr,] # this keeps only the rows with the filter combo
  # tmp2 is the data where only species that appear in 5 or more years are kept
  # because it is joined back with dat and not poscatch, it includes zero catches
  # it takes it down from 354047 records to 323200 records
  year.species= unique(tmp2$Espece)
  
  specs= fread("/home/daniel/database/Donnees_PACES/species.codes.csv")
  specs[grep(8093,specs$codeqc),c(7,9)]
  specs[grep(8093,specs$codeqc),]
  
  crap= dat[dat$Espece==2183 & dat$pds_pue>0,]
  unique(crap$Annee)
  
#### station filter
  # this is more difficult because station has no meaning across years:
  # Station 1 in one year is not the same as station 1 in another
  poscatch$spstn= paste(poscatch$Espece, poscatch$no_stn)
  stncount= aggregate(poscatch$spstn, list(poscatch$spstn),length