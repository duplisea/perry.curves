# get the data from Perry
  library(Perry.curves)
  subdata= biomass.w.zeros[year %in% 2018 & species %in% 438]
  z=subdata$biomass[order(subdata$temperature)]
  x= subdata$temperature[order(subdata$temperature)]
  y=z
  zz= data.frame(x=x,y=y)
  #zz= zz[zz$x>0,]
  plot(zz$x, zz$y, type="p")
  #z= na.trim(filter(z,rep(1/20,20),sides=2))
  #lines(z,col="blue",lwd=3)
  #z=diff(z)
  cumz= cumsum(zz$y)/sum(zz$y)
  #ansmeanvar=cpt.mean(z,method="PELT",Q=2)
  ansmean= cpt.mean(zz$y)
  #ansmean= cpt.meanvar(z,penalty="Asymptotic",pen.value=0.01,method="PELT",Q=3)
  plot(ansmean,cpt.width=3)
  abline(v=cpts(ansmean), col="grey")
  plot(zz$x,cumz)
  abline(v=zz$x[cpts(ansmean)], col="grey")

# Generate sample data with the Janoschek function
set.seed(123)
x <- seq(0, 10, by = 0.1)
A=1; B=2; M=3; TT=0.5
y <- 3 * (A + exp(-B * (x - M))) ^ (-1/TT) + rnorm(length(x), sd = 0.2) # Add some noise
plot(x,y)
lines(x, 3 * (A + exp(-B * (x - M))) ^ (-1/TT))

# Fit the Janoschek model
janoschek_fit <- nls(y ~ A * (1 + exp(-B * (x - M)))^(-1/TT), start = list(A = 2, B = 1, M = 3, TT = 0.8))

    # A: Asymptote
    # B: Growth Rate
    # M: Inflection Point
    # TT: Asymmetry

# View results
summary(janoschek_fit)
lines(x,predict(janoschek_fit), col="red")


# Richards function, modified to have an additive constant

set.seed(100)  # For reproducibility

# Define x values and function parameters
x <- seq(-2, 6, length.out = 150) 
A <- 0.9  # Asymptote
B <- 2    # Affects x-axis shift
K <- 0.8  # Growth rate
D <- 0.6  # Asymmetry
b <- 0.4  # Additive value where +y intercept

# Generate y values based on Richards function with noise
y <- b + A * (1 + exp((B - K * x) / D)) ** (-1/D) + rnorm(length(x), mean = 0, sd = 0.05)

# Ensure y values fall between 0 and 1
y <- pmax(0, pmin(1, y))   

# Create a dataframe
data <- data.frame(x=zz$x,y=cumz)
plot(data, pch=20)
start = list(A = 0.91, B = 2.1, K = 0.7, D = 0.8, b = 0.1)
rich.fit = nls(y ~ b + A * (1 + exp((B - K * x) / D)) ^ (-1/D), start=start, 
               algorithm = "port")
lines(x, predict(rich.fit), lwd=3, col="blue")

