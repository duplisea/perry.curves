# 23 November 2023 Update using Daniel code
# 
#
setwd("C:/PROJECTS/EA project/ECOLOGICAL INDICATORS/Tolerance/")
setwd("/home/daniel/database/Donnees_PACES/")
#
library(tidyverse)
library(ggplot2)
library(cowplot)
library(xlsx)
library(tidystats)
require(dplyr)
require(Hmisc)
require(tidyr)
require(ggplot2)
require(strucchange)
library(data.table)
 
# open data
capt	<- read.csv("NGSL_Capt.csv",header=T,sep=';')
head(capt)
dim(capt)
# [1] 179212     19
set <-read.csv("NGSL_Set.csv",header=T,sep=';')
head(set)
dim(set)
# [1] 7631   24
sp <-read.csv("Taxons_IML_20210114_v2.csv",sep=";")
 
### Keep trawling activity only
table(set$Engin,set$Annee)
set$Engin <-as.character(set$Engin)
set <- set[set$Engin %nin% c("99"),]
 
### keep successful tows only
set <- set[set$Resultat %in% c("1","2"),]
 
### Check distance trawled and Calculate swept area for each tow in km2
# 1- distance trawled
summary(set$Dist_vit)
summary(set$Dist_cor)
summary(set$Dist_pos)
set$dist<-ifelse(is.na(set$Dist_cor), set$Dist_vit, set$Dist_cor)
set$dist<-ifelse(is.na(set$dist), set$Dist_pos, set$dist)
summary(set$dist)
#    Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
# 0.1000  0.7500  0.8000  0.9223  1.2000  1.8500       1
 
# 2- horizontal opening
summary(set$Ouv_Hori)
# Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 13.41   13.41   16.71   15.24   16.94   16.94  # this is not a constant
set$swept.area.km2  <-(set$dist*1.852)*(set$Ouv_Hori*0.001)
summary(set$swept.area.km2)
#     Min.  1st Qu.   Median     Mean  3rd Qu.     Max.     NA's 
# 0.003137 0.023530 0.024471 0.025477 0.029802 0.045945        1
 
### check vessels and remove duplicates/comparative tows
table(set$nbpc,set$Annee)
x <-rbind(set[set$nbpc %in% "34" & set$Annee %in% c(1990:2003),],
          set[set$nbpc %in% "39" & set$Annee %in% c(2004:2021),],
          set[set$nbpc %in% "10" & set$Annee %in% c(2022:2023),])
table(x$nbpc,x$Annee)
set <-x
rm(x)
 
 
### 
head(set)
#####################################################
# Daniel Code from here
#####################################################
# tow <- fread(paste0(paces.dir,"NGSL_Set.csv")) 
tow <-set
names(tow) <- tolower(names(tow)) # for some reason whoever extracts the PACES data changes case from year to year
# fwrite(tow, file= paste0(raw.data.dir,"tow.rda")) # write the raw data
# tow <- tow[resultat %in% 1:2] # select only good sets
tow <- tow[,c("source", "no_rel", "nbpc", "no_stn", "annee", "tmp_fond","prof_moy","lon_deb","lat_deb","dist","swept.area.km2")]
tow <-as.data.table(tow)
setkeyv(tow, c("source", "no_rel","nbpc","no_stn")) # you need to use setkeyv if multivariate key otherwise just setkey is fine for one key column
 
# catch <- fread(paste0(paces.dir,"NGSL_Capt.csv"))
catch <-capt
names(catch) <- tolower(names(catch))
# fwrite(catch, file= paste0(raw.data.dir,"catch.rda")) # write the raw data
catch <- catch[,c("source", "no_rel","nbpc","no_stn","espece","pds_capt", "pds_capt_cor","pds_capt_cor_ca")] # select only certain columns
catch <-as.data.table(catch)
setkeyv(catch, c("source", "no_rel","nbpc","no_stn")) # set key variables (common to both tables) to join them.
test<-catch[tow]
# check catch vectors
tapply(test$pds_capt,test$annee,function(x) sum(x,na.rm=T))
tapply(test$pds_capt_cor,test$annee,function(x) sum(x,na.rm=T))
tapply(test$pds_capt_cor_ca,test$annee,function(x) sum(x,na.rm=T))
# decision to use cabot-equivalent corrected catch
rm(test)
# 
# MJR - First standardise for area sampled by each tow
x<-catch[tow]
summary(x$swept.area.km2)
x$std.catch <-x$pds_capt_cor_ca*median(x$swept.area.km2,na.rm=T)/x$swept.area.km2
 
## Then sum over categories by species within a tow
catch.std <- x[,.(biomass=sum(std.catch)), by= c("source", "no_rel","nbpc","no_stn","espece")] 
catch.std
rm(x)
# remove tow dist/area
tow <- tow[,c("source", "no_rel", "nbpc", "no_stn", "annee", "tmp_fond","prof_moy","lon_deb","lat_deb")]
# 
##################################################
# Join the data and select only year x species combinations that:
# have a threshold number of positive tows in a years
# no NA in the E variable
##################################################
# captured biomass with temperature joined to it and no NA
allbiomass <- tow[catch.std]
allbiomass
allbiomass <- na.omit(allbiomass) # captured biomass with temperature joined to it and no NA
 
# create a year x species column. We aggregate on this later
allbiomass$yrsp <- paste(allbiomass$espece,allbiomass$annee,sep="-")
setkey(allbiomass,yrsp)
 
##################################################
# filtering (change this yourself according to your needs)
##################################################
# the number of positive catch sets per year required in order to keep that year x species combo in the analysis.
no.sets.year <- 10
 
# the number of tows which caught a species by year
keepers <- aggregate(allbiomass$biomass, list(allbiomass$yrsp), length)
keepers <- as.data.table(keepers[keepers$x >= no.sets.year,])
names(keepers)= c("yrsp","ntows")
setkey(keepers, yrsp)
 
# join back to the allbiomass table keeping only threshold year x species
biomass <- allbiomass[keepers]
setkeyv(biomass, c("source", "no_rel","nbpc", "no_stn","espece"))
 
# join the set/tow longs and lats to the biomass table so they can be plotted on a map and keep 
#   just selected column.
biomass <-tow[biomass]
biomass <- biomass[,c("espece","annee","no_stn","biomass","tmp_fond","prof_moy","lon_deb","lat_deb")]
names(biomass) <- c("species","year","towno","biomass","temperature","depth","longitude","latitude")
setkeyv(biomass, c("species","year","towno"))
biomass
 
# Expand grid to give year x station x species combo zero catch if it was not caught
# Everything to this point was reducing the data down. This step will increase it again with zeros
# Note that if there was a set where absolutely nothing was caught in any of the year x species 
#   combos, it will not have an entry. i.e. it will be as though that tow never occurred. This 
#   would be rare (in fact I am not sure it occurs) and is feature not a bug. One could include
#   all stations by getting the station list by year from the original set table.
# Assigning zeros is the trickiest part of the filtering because stations are simply an order 
#   within a year and station 17 in 2008 does not correspond to station 17 in 2012. So you need to
#   expand the grid by year not over all years or you will get zeros for tows that never occurred,
#   and you will get zeros for stations in each year corresponding the year with the greatest
#   number of tows.
# I am sure there is an elegant way to do this in data.table but I need to loop it over years
# I don't think tapply will work as it will assume there is temporal constancy in the meaning of
#   no_stn. It may work if you really understand tapply
# This clunky chunk of code is clearly not my best piece of work
 
biomass.w.zeros= data.table(species=NA,year=NA,towno=NA,biomass=NA,temperature=NA,depth=NA,longitude=NA,latitude=NA)
years= 1990:2023 
#i <-2023
 
for (i in years){
  print(i)
  bmsyr <- biomass[year==i] # select just one year of data at a time
  towno <- unique(bmsyr$towno) # get tow numbers for that year. This is where you would go to the original set table to expand the grid to tows that didn't catch anything at all
  spec <- unique(bmsyr$species) # a vector of the species caught in that year
  expanded <- as.data.table(expand.grid(year=i, towno=towno, species= spec)) # this gives frame where existing biomass can be inserted and the unfilled ones get filled with zero
  setkeyv(expanded, c("species","year","towno"))
  bms.expanded <- bmsyr[expanded]
  bms.expanded$biomass[is.na(bms.expanded$biomass)] <- 0
  setkeyv(bms.expanded, c("year","towno"))
  physical <- bmsyr[,c("year","towno","temperature","depth","longitude","latitude")]
  physical <- physical[!duplicated(physical),] # select temp and depth to join back to zero catch rows
  bms.expanded <- bms.expanded[physical] # this is good to inspect to make sure the temperature and depth columns correspond
  bms.expanded <- bms.expanded[,c("species","year","towno","biomass","i.temperature","i.depth","i.longitude","i.latitude")]
  names(bms.expanded) <- c("species","year","towno","biomass","temperature","depth","longitude","latitude")
  biomass.w.zeros <- rbind(biomass.w.zeros, bms.expanded)
}
biomass.w.zeros <- biomass.w.zeros[-1,]
 save(biomass.w.zeros, file= "biomass.w.zeros.rda")
# import the species table
# species.table <- fread(paste0(paces.dir,"species.codes.csv"))
#save(sp, file= "species.rda")
 
rm(i,spec,towno,years,physical,keepers,expanded,bmsyr,bms.expanded,no.sets.year)
 
 
#####################################################
# devtools::install_gitlab("duplisea/Perry.curves")
library(Perry.curves)
 
# the filtered data
dat <-biomass.w.zeros
dat
 
 
### exemples from Dan email 20.11.2023
# examples of how to find species codes by keywords
find.species.f("squid")
find.species.f(8173)
find.species.f("polar")
 
# construct a single set of Perry curves and compute breakpoints
# select the data
subdata= dat[year %in% c(2023) & species %in% 8173]
# run the analysis
Perry.f(subdata, plot=T)
rm(subdata)
 
# get the breakpoints for several years and species. You can do it for all species and all years if you want
# subdata= biomass.w.zeros[year %in% 1990:2022 & species %in% c(438, 892, 889)]
# Perry.breaks <- Perry.loop.f(subdata, p1=0.1, p2=0.9) # where p1 and p2 is the E at percentile 1 and 2.
# all.perry <- Perry.loop.f(biomass.w.zeros, p1=0.1, p2=0.9) # all species and all years, it will take a few minutes to run
 
subdata = dat[year %in% 1990:2023 & species %in% unique(dat$species)]
all.perry = Perry.loop.f(subdata, p1=0.25, p2=0.75)
head(all.perry)
 
#########
length(unique(all.perry$species))
# [1] 244
length(unique(all.perry$year))
# [1] 34
 
# count of species per year
barplot(tapply(all.perry$species,all.perry$year, function(x) length(unique(x))))
# count of species sampled in all years
x <-as.data.frame(aggregate(all.perry$year, list(all.perry$species), function(x) length(unique(x))))
names(x) <-c("sp","n.yr")
nrow(x[x$n.yr==34,])
# [1] 19 #species sampled in all years including 9995 = digested invertebrates (remove) and a number of non-commercial species (those are particularily interesting)
 
## save for now 
write.csv(all.perry, file = "deltaT_19902023.csv", row.names = F)
 
 
# you can then plot the Perry.breaks output as it save it to a data.frame by species
# find.species.f(9995)
# sp[sp$Norme_Strap_IML %in% 13,]
# spec.row= all.perry[all.perry$species %in% 13,] 
# plot(all.perry$year[spec.row],all.perry$Ezone.bp[spec.row],type="l")