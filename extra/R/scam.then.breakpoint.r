# get the Perry curve for a species and year
# fit a shape constrained gam to monotonic increasing to the cumulative Perry curve
# predict the gam for evenly spaced E
# difference the prediction to get a numerical derivative of the curve
# apply the meanvar changepoint method to the numerical derivative to find
# breakpoints

# to do
# find a single breakpoint in 2 parts of the curve [0,0.2] of biomass in the
# cumulative distribution and [0.8,1]. The will ensure that breakpoints will not
# be described in the middle part of the curve where it makes no sense

library(scam)
library(changepoint)

# Sample Data
set.seed(123) 
x <- seq(0, 5, length.out = 100)
y <- plogis(x - 2.5) + rnorm(length(x), sd = 0.05)  # Simulate increasing CDF data
plot(x,y,pch=20)

# Fit the SCAM model 
model <- scam(y ~ s(x, bs = "ps"), family = gaussian()) 
lines(x,predict(model))

########################
  library(Perry.curves)
  find.species.f("plaice")
  subdata= biomass.w.zeros[year %in% 1997 & species %in% 889]
  Perry.f(subdata, plot=T)

  z=subdata$biomass[order(subdata$temperature)]
  x= subdata$temperature[order(subdata$temperature)]
  y= cumsum(z)/sum(z)
  plot(x, y, pch=20)
  
  model <- scam(y ~ s(x, bs = "ps"), family = gaussian()) 
  lines(x,predict(model),lwd=2, col="blue")

  # generate evenly spaced data from the scam fit
  newdata= data.frame(x=seq(min(x),max(x),length=200))
  gampred <- predict(model, newdata=newdata)
  first.der= diff(gampred)
  plot(first.der)
  
  # fit the changepoint function to the derivative
  ansmean= cpt.meanvar(first.der)
  cpts(ansmean)
  plot(newdata$x,gampred, type="l",lwd=2)
  abline(v=newdata$x[cpts(ansmean)], col="grey")
  
  #####################################
  
  bp.f(x=x, y=y, percentile.search=0.01)
  