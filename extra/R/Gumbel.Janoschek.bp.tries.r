# breakpoints, Perry

# fit a gompertz to the cumulative distribution
# calculate y values for equally spaced x
# difference the y values for a numerical derivative
# find the points of maximum curvature
# add some kinds of error checking for places where the y intercept is not zero
# and the curve does not asymptote

# get the data from Perry
  library(Perry.curves)
  subdata= biomass.w.zeros[year %in% 2018 & species %in% 438]
  z=subdata$biomass[order(subdata$temperature)]
  x= subdata$temperature[order(subdata$temperature)]
  y=z
  zz= data.frame(x=x,y=y)
  zz= zz[zz$x>0,]
  plot(zz$x, zz$y, type="p")
  #z= na.trim(filter(z,rep(1/20,20),sides=2))
  #lines(z,col="blue",lwd=3)
  #z=diff(z)
  cumz= cumsum(zz$y)
  #ansmeanvar=cpt.mean(z,method="PELT",Q=2)
  ansmean= cpt.mean(zz$y)
  #ansmean= cpt.meanvar(z,penalty="Asymptotic",pen.value=0.01,method="PELT",Q=3)
  plot(ansmean,cpt.width=3)
  abline(v=cpts(ansmean), col="grey")
  plot(zz$x,cumz)
  abline(v=zz$x[cpts(ansmean)], col="grey")
  
# Fit the Gompertz
  start = list(a = 1, b = 3, c = 0.8) 
  gomp <-(nls(cumz~a*exp(-b*exp(-c*zz$x)), start=start))
  summary(gomp)
  param= coef(gomp)
  G2= param[1]*exp(-param[2]*exp(-param[3]*xx))+10
  lines(xx, G2,col="blue",lwd=2)
  xinflection= log(param[2])/param[3]
  yinflection= param[1]*exp(-param[2]*exp(-param[3]*xinflection))+10
  
  # Gumbel function
  nls(cumz ~ exp(-exp(-(zz$x - mu) / beta)), start = list(mu = 3, beta = 3))

  # Janoschek Growth Function
  nls(cumz ~ A * (1 + exp(-B * (zz$x - M)))^(-1/T), start = list(A = 1, B = .1, M = 2.5, T = 0.7)) 
  