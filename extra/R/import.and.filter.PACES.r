# bringing in new data from PACES and write cleaned and filtered data to the package data directory

library(data.table)

##################################################
# switches (change this yourself according to your needs)
##################################################
  # the number of positive catch sets per year required in order to keep that year x species combo in the analysis.
  no.sets.year <- 10 

##################################################
# directories (replace with your own)
##################################################
  paces.dir <- "/home/daniel/database/Donnees_PACES/"
  raw.data.dir <- "extra/data/" #relative to your working directory

##################################################
# load the PACES data
##################################################
  tow <- fread(paste0(paces.dir,"NGSL_Set.csv")) 
    names(tow) <- tolower(names(tow)) # for some reason whoever extracts the PACES data changes case from year to year
    fwrite(tow, file= paste0(raw.data.dir,"tow.rda")) # write the raw data
    tow <- tow[resultat %in% 1:2] # select only good sets
    tow <- tow[,c("source", "no_rel", "nbpc", "no_stn", "annee", "tmp_fond","prof_moy","lon_deb","lat_deb")]
    setkeyv(tow, c("source", "no_rel","nbpc","no_stn")) # you need to use setkeyv if multivariate key otherwise just setkey is fine for one key column
    
  catch <- fread(paste0(paces.dir,"NGSL_Capt.csv"))
    names(catch) <- tolower(names(catch))
    fwrite(catch, file= paste0(raw.data.dir,"catch.rda")) # write the raw data
    catch <- catch[,c("source", "no_rel","nbpc","no_stn","espece","pds_capt_cor_ca")] # select only certain columns
    catch <- catch[,.(biomass=sum(pds_capt_cor_ca)), by= c("source", "no_rel","nbpc","no_stn","espece")] # sum over the categories by species within a tow
    setkeyv(catch, c("source", "no_rel","nbpc","no_stn")) # set key variables (common to both tables) to join them.
      
    
##################################################
# Join the data and select only year x species combinations that:
  # have a threshold number of positive tows in a years
  # no NA in the E variable
##################################################
  # captured biomass with temperature joined to it and no NA
    allbiomass <- catch[tow]
    allbiomass <- na.omit(allbiomass) # captured biomass with temperature joined to it and no NA
    
  # create a year x species column. We aggregate on this later
    allbiomass$yrsp <- paste(allbiomass$annee, allbiomass$espece)
    setkey(allbiomass,yrsp)
    
  # the number of tows which caught a species by year
    keepers <- aggregate(allbiomass$biomass, list(allbiomass$yrsp), length)
    keepers <- as.data.table(keepers[keepers$x >= no.sets.year,])
    names(keepers)= c("yrsp","ntows")
    setkey(keepers, yrsp)
    
  # join back to the allbiomass table keeping only threshold year x species
    biomass <- allbiomass[keepers]
    setkeyv(biomass, c("source", "no_rel","nbpc", "no_stn","espece"))
    
  # join the set/tow longs and lats to the biomass table so they can be plotted on a map and keep 
  #   just selected column.
    biomass <-tow[biomass]
    biomass <- biomass[,c("espece","annee","no_stn","biomass","tmp_fond","prof_moy","lon_deb","lat_deb")]
    names(biomass) <- c("species","year","towno","biomass","temperature","depth","longitude","latitude")
    setkeyv(biomass, c("species","year","towno"))
    
  # Expand grid to give year x station x species combo zero catch if it was not caught
  # Everything to this point was reducing the data down. This step will increase it again with zeros
  # Note that if there was a set where absolutely nothing was caught in any of the year x species 
  #   combos, it will not have an entry. i.e. it will be as though that tow never occurred. This 
  #   would be rare (in fact I am not sure it occurs) and is feature not a bug. One could include
  #   all stations by getting the station list by year from the original set table.
  # Assigning zeros is the trickiest part of the filtering because stations are simply an order 
  #   within a year and station 17 in 2008 does not correspond to station 17 in 2012. So you need to
  #   expand the grid by year not over all years or you will get zeros for tows that never occurred,
  #   and you will get zeros for stations in each year corresponding the year with the greatest
  #   number of tows.
  # I am sure there is an elegant way to do this in data.table but I need to loop it over years
  # I don't think tapply will work as it will assume there is temporal constancy in the meaning of
  #   no_stn. It may work if you really understand tapply
  # This clunky chunk of code is clearly not my best piece of work

    biomass.w.zeros= data.table(species=NA,year=NA,towno=NA,biomass=NA,temperature=NA,depth=NA,longitude=NA,latitude=NA)
    #years=unique(biomass$year) 
    years= 1990:2021
    #i=2006
    
    for (i in years){
      print(i)
      bmsyr <- biomass[year==i] # select just one year of data at a time
      towno <- unique(bmsyr$towno) # get tow numbers for that year. This is where you would go to the original set table to expand the grid to tows that didn't catch anything at all
      spec <- unique(bmsyr$species) # a vector of the species caught in that year
      expanded <- as.data.table(expand.grid(year=i, towno=towno, species= spec)) # this gives frame where existing biomass can be inserted and the unfilled ones get filled with zero
      setkeyv(expanded, c("species","year","towno"))
      bms.expanded <- bmsyr[expanded]
      bms.expanded$biomass[is.na(bms.expanded$biomass)] <- 0
      setkeyv(bms.expanded, c("year","towno"))
      physical <- bmsyr[,c("year","towno","temperature","depth","longitude","latitude")]
      physical <- physical[!duplicated(physical),] # select temp and depth to join back to zero catch rows
      bms.expanded <- bms.expanded[physical] # this is good to inspect to make sure the temperature and depth columns correspond
      bms.expanded <- bms.expanded[,c("species","year","towno","biomass","i.temperature","i.depth","i.longitude","i.latitude")]
      names(bms.expanded) <- c("species","year","towno","biomass","temperature","depth","longitude","latitude")
      biomass.w.zeros <- rbind(biomass.w.zeros, bms.expanded)
    }
    biomass.w.zeros <- biomass.w.zeros[-1,]
    save(biomass.w.zeros, file= "data/biomass.w.zeros.rda")

# import the species table
    species.table <- fread(paste0(paces.dir,"species.codes.csv"))
    save(species.table, file= "data/species.rda")
    