# smooth edata and put on graph
library(Perry.curves)
  subdata= biomass.w.zeros[year %in% 2011:2011 & species %in% 8511]
  library(mgcv)
  subdata$x= -decdeg(subdata$longitude*100)
  subdata$y= decdeg(subdata$latitude*100)
  tgam= gam(temperature~ te(x, y), data=subdata)
  newdat= data.frame(x= -decdeg(biomass.w.zeros[,longitude]*100),
                     y= decdeg(biomass.w.zeros[,latitude]*100))
  newdat$smooth= predict(tgam, newdata=newdat)
#  subdata$smooth[subdata$smooth<0]=0

  library(scales)
  newdat$smooth= rescale(newdat$smooth,c(1,100))
  
  #colgrad= rev(heat.colors(100))
  
  # Create a function that generates the color gradient
  color_gradient <- colorRampPalette(c("blue", "green","yellow","red"), interpolate="spline")
  # Generate the color gradient
  colgrad <- color_gradient(100)

  
  map.f()
  #points(newdat$x,newdat$y,col=newdat$smooth+5, cex=.5, pch="20")
  #points(newdat$x,newdat$y,col=colgrad[newdat$smooth], pch=".")
  points(newdat$x,newdat$y,col=colgrad[newdat$smooth], cex=.5, pch=20)
#  points(subdata$x,subdata$y,col=subdata$temperature, cex=0.6, pch=20)

          catches= data.frame(EID=1:length(subdata$x), X=subdata$x, Y=subdata$y, Z=subdata$biomass)
          fishing.events= as.EventData(catches)
          #points(x, y, col=C, pch=20)
          addBubbles(fishing.events,type="perceptual", symbol.bg=rgb(.9,.5,0,0), 
                     legend.type="nested", symbol.zero="+",
                     legend.title="Biomass")
          
map.E.color= function(subdata){
  library(mgcv)
  library(scales)

  # fit a 2 d spatial gam to interpolate temperature
  subdata$x= -decdeg(subdata$longitude*100)
  subdata$y= decdeg(subdata$latitude*100)
  tgam= gam(temperature~ te(x, y), data=subdata)
  
  # make new data so the surface is smooth. Do this by taking lat and long for
  # every tow ever fished and predict what the temperature would have been for that tow
  # in the year your data are from.
  newdat= data.frame(x= -decdeg(biomass.w.zeros[,longitude]*100),
                     y= decdeg(biomass.w.zeros[,latitude]*100))
  newdat$smooth= predict(tgam, newdata=newdat)
  
  # rescale the predicted temperature values between 1 and 100
  newdat$smooth= rescale(newdat$smooth,c(1,100))
  
  # Create the color gradient colur scheme
  color_gradient <- colorRampPalette(c("blue", "green", "yellow", "red"), 
                                     interpolate="spline")
  
  # Generate the color gradient for 100 colours
  colgrad <- color_gradient(100)
  
  # plot the colour gradient as points
  points(newdat$x,newdat$y,col=colgrad[newdat$smooth], cex=0.6, pch=20)
}

map.f()
map.E.color(subdata)
