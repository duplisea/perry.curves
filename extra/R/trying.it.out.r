# Test

# to do
  # put in a flag for when the bp is estimated or not
  # compare my curves with a weighted catch curve
  # investigate why there are NA produced in the bp function

# load up the filtered data
  library(Perry.curves)
  #library(ecan.map)

# test of the single year and species Perry function: Perry.f
  find.species.f("squid")
  find.species.f(8111)
  subdata= biomass.w.zeros[year %in% 2017:2021 & species %in% 4753]
  subdata= biomass.w.zeros[year %in% 2019 & species %in% 438]
  #crap= aggregate(biomass.w.zeros$biomass, list(biomass.w.zeros$year,biomass.w.zeros$species),range)
  #apply(crap,2,range)
  Perry.f(subdata, percentile.search=0.25, T.rescaling=2, plot=T, E.field=F)
  Perry.E.weighted.catch.f(subdata, percentile.search=0.25, plot=T, E.field=F)
  
  ## 18 Nov 2023 observations
  ## am I actually calculating the Perry curves properly, they sort of don't look right?
  ## Laurie checked a couple of cases against her code in March 2024 and they seem correct.

# try the Perry loop
  subdata= biomass.w.zeros[year %in% 1990:2020 & species %in% c(438)]
  tmp=Perry.loop.f(subdata, T.rescaling=2, p1=0.05, p2=0.95)
  
# try the Perry loop on any species with a code <1000
  species.lt1000= unique(biomass.w.zeros$species)[unique(biomass.w.zeros$species)<1000]
  subdata= biomass.w.zeros[year %in% 1990:2022 & species %in% species.lt1000]
  perry.all <- Perry.loop.f(subdata,T.rescaling=2,percentile.search=0.15, p1=0.05, p2=0.95)
  
  plot(perry.all$p1,perry.all$Ezone.p, 
       xlab="Temperature at 0.05 quantile of cumulative catch", 
       ylab="Delta t, quantile method",
       main="All species with codes <1000, 1990:2022")

  plot(perry.all$p2,perry.all$Ezone.p, 
       xlab="Temperature at 0.95 quantile of cumulative catch", 
       ylab="Delta t, quantile method",
       main="All species with codes <1000, 1990:2022")
  
# loop by species all years
  subdata <- biomass.w.zeros[year %in% 1999 & species %in% 792]
  spec.all <- Perry.loop.f(subdata, p1=0.1, p2=0.9)
  plot(spec.all$year, spec.all$Ezone.bp)

# 1 year all species  
  subdata <- biomass.w.zeros[year %in% 2010]
  spec.all <- Perry.loop.f(subdata, p1=0.1, p2=0.9)
  plot(spec.all$species, spec.all$Ezone.bp, xlab="Species code", ylab="Delta T, breakpoint method" )
  plot(density(na.exclude(spec.all$Ezone.bp)))
  # 13 150 444 447 478 572 696 792 # these species have an E zone >10 in 1999
  spec=c(13,150,444,447,478,572,696,792)
  species.table$English[species.table$codeqc  %in% spec]

# temperature range over years
  tapply(biomass.w.zeros$temperature,biomass.w.zeros$year,range)
  # something weird in 1994 and 1999
  
# 1999 remove station 72
  subdata <- biomass.w.zeros[year %in% 1999 & species %in% 792 & towno %notin% 72]
  Perry.f(subdata, p1=0.1, p2=0.9, plots=T)
  plot(spec.all$year, spec.all$Ezone.bp)
  
# survey stations 2009-
  subdata <- biomass.w.zeros[year %in% 2009:2023]
  subdata <- subdata[,7:8]
  subdata <- unique(subdata) # unique station stations by year
  map.f()
  stat.area.f("ngsl.trawl.survey.strata")
  points(-decdeg(subdata$longitude),decdeg(subdata$latitude),pch=20,
         col="blue",cex=0.1)
  
  
# species 728 in 2010
  find.species.f(728)
  subdata= biomass.w.zeros[year %in% 2010 & species %in% 728]
  Perry.f(subdata, percentile.search=0.25, plot=T)
