setwd("~/tmp/MJbreakpoint")

############ Libraries used
library(data.table)
library(strucchange)


############ import the data and get rid of NA
dat= fread("dat.csv")
dat= na.omit(dat)


#################
# filter the data. DO NOT USE THIS. IT IS NOT WORKING AS EXPECTED
#################
survey.filter= function(dat, no.years=5, no.stations=10){

  # select only positive catches for filtering
    poscatch= dat[pds_pue>0,]

  # Determine the stations and years meeting the criteria in "poscatch"
    criteria_stations_years <- poscatch[, .(num_years = uniqueN(Annee), num_stations = uniqueN(no_stn)), by = .(Espece)][num_years >= no.years & num_stations >= no.stations]
  
  # Filter the larger data.table "dat" based on the determined stations and years with positive catches
    filtered_dat <- dat[no_stn %in% criteria_stations_years$Espece, .(Annee, no_stn, Espece, pds_pue, Tmp_fond, prof_moy)]
  
    filtered_dat
}



#################
# breakpoint function 
#################
bp.f= function(x, y){
  bp= breakpoints(y ~ x, breaks=2) 
  bp.x= bp$breakpoints
  bps= x[bp.x]
  if (length(bps)==1) bps= c(bps,NA) # if only one breakpoint is found, create an NA for the second one
  if (length(bps)>2) bps= c(NA,NA)
  bps
}


#################
# calculate the E at percentiles. This is moved into a function so I can use tryCatch for error detection
#################
Perry.percentiles= function(x,y, P1=0.25, P2=0.75){
  # x is cumulative catch and y in the environmental variable
  pos25= which.min(abs(x - P1))
  pos75= which.min(abs(x - P2))
  E.percentiles=c(-888,-888)
  E.percentiles[1:2]= y[c(pos25, pos75)]
  E.percentiles
}

#################
# calculate the E at percentiles. This is moved into a function so I can use tryCatch for error detection
#################
Perry.percentiles= function(x,y, P1=0.25, P2=0.75){
  # x is cumulative catch and y in the environmental variable
  pos25= which.min(abs(x - P1))
  pos75= which.min(abs(x - P2))
  E.percentiles=c(-888,-888)
  E.percentiles[1:2]= y[c(pos25, pos75)]
  E.percentiles
}
  
#################
# the Perry curve and breakpoint calculation
#################
Perry.f= function(data, p1=0.05, p2=0.95, plots=FALSE){
  # you do not need to standardise it but just to keep the plotting consistent
  # and so that people do not get drawn in by the biomass value as the relationship
  # between how biomass changes relative to E is all that we are interested in
    stn.catch= data$pds_pue
    stn.E= data$Tmp_fond
    std.catch= stn.catch/sum(stn.catch)
    
  # order the series by E (smallest to largest)  
    ord= order(stn.E)
    stn.E= stn.E[ord]
    std.catch= std.catch[ord]

  # Cumulate the catch in order of increasing E
    cum.catch.E= cumsum(std.catch)
    
  # Cumulative E ordered from smallest to largest
    cum.E= cumsum(stn.E/sum(stn.E))
        
  # E at maximum difference between Perry curves
    max.diff= max(cum.catch.E-cum.E)
    pos.max.diff= which.min(abs(max.diff - cum.catch.E-cum.E))
    if(!is.nan(max.diff)) max.diff.E= stn.E[pos.max.diff] else max.diff.E= -777

  # find the breakpoints of the cumulative catch
    # catch errors so it does not break if used in a loop
    bps=c(-999,-999)
    tryCatch(bps<- bp.f(stn.E, cum.catch.E), error=function(e){return(c(NA,NA))}) # don't stop if there is an error and return NA
    Ezone.bp= diff(bps)
    
  # find the E at the p1th and p2th percentiles of cumulative catch
    # catch errors so it does not break if used in a loop
    tryCatch(E.percentiles<- Perry.percentiles(x=cum.catch.E, y=stn.E, P1=p1, P2=p2), error=function(e){return(c(NA,NA))}) # don't stop if there is an error. bps is by default at error (-888)
    Ezone.p= diff(E.percentiles)

  # make plots  
    if(plots){
      par(mfcol=c(2,1))
      plot(stn.E, std.catch, type="p",pch=20, col="black", xlab="E variable", 
           ylab="Standardised catch per station")
      E.series=  data.frame(stn.E= seq(min(stn.E), max(stn.E),length=1000))
      lines(predict(smooth.spline(stn.E, std.catch),x=E.series$stn.E),col="blue",lwd=2)

      plot(stn.E, cum.catch.E, type="l", col="blue", xlab="E variable", 
           ylab="Cumulative catch", ylim=c(0,1), lwd=2)
      lines(stn.E, cum.E,  lwd=2, col="grey")
      # vertical lines for breakpoints and E at max diff on the graph
      abline(v=bps, lty=2, col="green")
      abline(v=E.percentiles, lty=2, col="red")
      abline(v=max.diff.E, col="black", lty=1)
      legend("topleft", lty=c(1,1,2,2,1), col=c("grey","blue","green","red","black"),bty="n",cex=0.7,
             legend=c("cumulative E","cumulative catch","breakpoints","percentiles","max difference"))
    }
  E.breaks= c(bps, E.percentiles, Ezone.bp, Ezone.p, max.diff.E)
  names(E.breaks)= c("bp1", "bp2", "p1", "p2", "Ezone.bp", "Ezone.p","max.diff.E")
  E.breaks
}



################
# The function to loop over years and species combinations
################
Perry.loop.f= function(data, p1, p2){
  # don'tuse tapply. debugging hell. Assumes a full matrix
    tmp= tapply(data, list(data$Espece, data$Annee), Perry.f, p1=p1, p2=p2, simplify=TRUE)
    
  # need to unlist it and turn it into a data.frame so we can work with it
    breaks= as.data.frame(matrix(unlist(tmp), ncol=7, byrow=TRUE))
    names(breaks)= c("bp1", "bp2", "p1", "p2", "Ezone.bp", "Ezone.p","max.diff.E")
    
  # because tapply tries to do it for continuous years, need to remove output computed on non-existent data
    remove= apply(breaks[,1:4],1,sum,na.rm=TRUE)!=-3774 #-3774 is sum of error codes when all columns in a row are errors  
    breaks= breaks[remove,]

  # get all the species year combos and put them as the first two columns so you know what the 
  # breakpoints are referring to
    year.spec= aggregate(pds_pue~ Annee + Espece, data, mean)[,1:2]
    breakpoints= cbind(year.spec, breaks)
    
    breakpoints
}

################# some data selection examples 

  years= sample(unique(dat$Annee), 10) # randomly select 8 years to try
  species= sample(unique(dat$Espece), 1) # randomly select 2 species to try, 8*2 = 16 Perry curves
  data= dat[dat$Espece %in% species & dat$Annee %in% years & !is.na(dat$Tmp_fond), ]
  data= data[data$pds_pue>0,] # this is dangerous but I just did it to try. It eliminates all zero stations

################# run the loop function on all data
#  dat= survey.filter(dat, 5, 10)  

  Perry.loop.f(dat)
  
#################
#   [1]   1 100 102  13  15 150 151 152 168 187 188 193  20 202 205 208 214 220 222 225 227 230 234  24
#  [25] 244  27 271 272 273 275 278 280 281 285 290 304 320 368 369 373 398 422 426 430 436 437 438 439
#  [49] 441 442 443 444 447 449 451 453 454 455 461 478 484 527 572 614 618 695 696 699 700 701 710 711
#  [73] 715 716 717 718 721 725 726 727 728 730 733 734 745 746 747 750 752 783 792 793 797 808 809 810
#  [97] 811 812 813 814 815 817 818 819 820 823 829 830 831 832 835 836 837 838 844 845 847 849 853 854
# [121] 856 857 859 862 865 867 868 874 882 887 889 890 891 892 893 895  90  91  94 966 982 984
  
################ New filter try
  data2= dat[dat$pds_pue>0, ]
  data3= aggregate(data2$pds_pue,list(year=data2$Annee,species=data2$Espece),length)
  names(data3)=c("year","species","number.of.stations")
  data3$yrsp= paste(data3$year,data3$species)
  selected.yrsp= as.data.table(data3[data3$number.of.stations>=10,])
  setkey(selected.yrsp,yrsp)
  
  dat$yrsp = paste(dat$Annee, dat$Espece)
  setkey(dat, yrsp)
  dat.selected= dat[selected.yrsp]
  crappo=Perry.loop.f(dat.selected)

#################
  unique(dat.selected$Espece)
################# Try for just one species (792) and 1 year and make the curves
  specs= fread("/home/daniel/database/Donnees_PACES/species.codes.csv")
  search.term="capelin"
  as.data.frame(specs[grep(search.term,specs$English),c(7,9)])
  specs[grep(search.term,specs$codeqc),c(7,9)]
  dat.1spec= dat.selected[dat.selected$Espece %in% 187 & dat.selected$Annee %in% 2020 & !is.na(dat.selected$Tmp_fond), ]
  Perry.f(dat.1spec, plots=T)
  
  
  
  
  
  
  
  # KEEP LINES ABOVE  
  
  
  
  
  
  
  
  
  
  
  
  speciesxx= sample(unique(dat.selected$Espece), 2)  
  speciesxx=187
  data= dat.selected[dat.selected$Espece %in% speciesxx,]
  loop.test= Perry.loop.f(data)
  
  data2= data[data$Annee==2008,]
  Perry.f(data2, plots=F)
  
  data2= data2[order(data2$Tmp_fond),]
  cum.pds=cumsum(data2$pds_pue)/sum(data2$pds_pue)
  plot(data2$Tmp_fond,cum.pds)
  bp.f(data2$Tmp_fond,cum.pds)
  
  
  speciesxx= unique(dat.selected$Espece)
  for(i in speciesxx){
    i
    data= dat.selected[dat.selected$Espece==i,]
    Perry.loop.f(data)
  }

  
  ############### to do
  # filter data
  # Make a fcuntion to plot results and T at max diff
