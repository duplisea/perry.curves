#################
# the Perry curve unweighted catch and breakpoint calculation
#################
#' Analyze Catch Distribution and Environmental Preferences
#'
#' This function analyzes the relationship between standardized catch distribution 
#' and an environmental variable. It calculates Perry curves, breakpoints, environmental 
#' percentiles, and the difference between cumulative curves to assess environmental preferences.
#'
#' @param data A data frame containing the following columns:
#'   * 'biomass':  Catch biomass at each sampling location.
#'   * 'temperature': Environmental variable values (e.g., temperature) at each location.
#'   * 'longitude': Sampling location longitude (used for optional mapping).
#'   * 'latitude': Sampling location latitude (used for optional mapping).
#'   * 'species': Species name (used for plot labeling).
#'   * 'year': Year of sampling (used for plot labeling).
#' @param percentile.search A numeric value between 0 and 0.5 (default: 0.25) specifying the  percentiles for breakpoint search.
#' @param p1 Numeric value between 0 and 1 (default: 0.05) specifying the lower
#'           percentile of the catch distribution for analysis.
#' @param p2 Numeric value between 0 and 1 (default: 0.95) specifying the upper 
#'           percentile of the catch distribution for analysis.
#' @param T.rescaling the method to handles temperatures <0. The problem with negative 
#'           temperature comes when creating the cumulative curve (which by definition 
#'           is strictly non-decreasing). 
#'           * Method 1 sets all temperature values <0 to 0.
#'           * Method 2 (default) shifts the entire temperature series to the right by the 
#'           smallest negative value, i.e. minimum temperature becomes 0.
#'           * Method 3 just truncates all the data thus removing all lines where 
#'           temperature is <0.
#' @param plots Logical (default: FALSE). If TRUE, generates a series of plots:
#'    * Catch vs. Environmental Variable
#'    * Perry Cumulative Curves
#'    * Map of catch distribution (requires additional spatial packages) 
#' @param E.field Logical (default: FALSE). If TRUE, includes an environmental 
#'                field in the map plot. Note: Significantly increases plotting time.
#'
#' @return  A named vector containing:
#'   * 'bp1', 'bp2': Breakpoints in the cumulative catch distribution.
#'   * 'p1', 'p2': Environmental variable values at the specified percentiles (`p1`, `p2`) of the catch distribution.
#'   * 'Ezone.bp', 'Ezone.p': Difference in environmental values between breakpoints and percentiles,  respectively.  
#'   * 'max.diff.E': Environmental value where the difference between Perry curves is maximized.
#'   * 'cumcatch.at.bp1', 'cumcatch.at.bp2': The proportion of cumulative catch at breakpoints.
#'   * 'cumcatch.at.p1', 'cumcatch.at.p2': The proportion of cumulative catch at percentiles.
#'   * 'bp1.flag', 'bp2.flag': were the bps estimated or just min and max of series (1=estimated)
#' @references Perry, R.I., and S.J. Smith. 1994. Identifying habitat associations of marine fishes using survey data: an application to the northwest Atlantic. Can. ). Fish. Aquat. Sci. 51: 589-602.
#' @export
#' @examples
#' subdata= biomass.w.zeros[year %in% 1997 & species %in% 889] # American plaice 1997
#' # Basic Usage
#' Perry.f(subdata) 
#'
#' # Generate Plots
#' Perry.f(subdata, plots = TRUE) 
Perry.f= function(data, percentile.search=0.25, p1=0.05, p2=0.95, T.rescaling=2, plots=FALSE, E.field=FALSE){
  # pull out basic data series with new names
    biomass= data$biomass
    stn.E= data$temperature
    std.catch= biomass/sum(biomass)
    
  # Temperature shifting options (i.e. problem that arrises when cumulating negative temperatures)
    if (T.rescaling==1){ # make all temperature <0 equal to 0. Ecologically 0 is about equivalent to -1.
      stn.E[stn.E<0]= 0
      rescaling= 0
    }    
    if (T.rescaling==2){ # shift the series by the minumum temperature unless the minumum temperature >=0
      rescaling= abs(min(0, min(data$temperature))) # acts like an if statement but faster. If temperatures go below 0 then it rescales the series to zero. Otherwise is makes the additive rescaling factor = 0.
    }    
    if (T.rescaling==3){ # truncate the data so that any data <0 are simply removed
      data= data[data$temperature >= 0,]
      biomass= data$biomass
      stn.E= data$temperature
      std.catch= biomass/sum(biomass)
      rescaling= 0
    }
    # if (T.rescaling==4){ # compute thermal energy content of 1g of water and transform the temperature series accordingly
    #   biomass= data$biomass
    #   stn.E= (data$temperature5+273.15)*4.184/1000 # convert to energy content in kilojoules for 1 g water. Specific heat capacity of water: 4.184 J/g°C
    #   rescaling= 0
    #   std.catch= stn.E*biomass/sum(biomass*stn.E)
    # }    
    
    # if (T.rescaling==5){ # min-max scale
    #   biomass= data$biomass
    #   stn.E = (data$temperature - min(data$temperature)) / (max(data$temperature) - min(data$temperature))
    #   rescaling= 0
    #   std.catch= stn.E*biomass/sum(biomass*stn.E)
    # }    
    
    
  # order the series by E (smallest to largest)  
    ord= order(stn.E)
    stn.E <- stn.E[ord]
    #stn.E.reg <<- stn.E
    std.catch= std.catch[ord]

  # Cumulate the catch in order of increasing E
    cum.catch.E <- cumsum(std.catch)
    #cum.catch.E.ref <<- cum.catch.E
    
  # Cumulative E ordered from smallest to largest. Shifted to make sure negative temperature are 0 or more. Preserves the ordering but avoid cumulating negative values
    cum.E <- cumsum((stn.E+rescaling)/sum(stn.E+rescaling))
    #cum.E.reg <<- cum.E

  # E at maximum difference between the two curves (E cumulated, catch cumulated. Both cumulated in order of increasing E)
    pos.max.diff= which.max(abs(cum.catch.E-cum.E))
    if(!is.nan(pos.max.diff)) max.diff.E= stn.E[pos.max.diff] else max.diff.E= -777
    
  # Difference of the area between the two cumulative curves. experimental
    # If positive, it is cold water selecting and if negative it is warm water selecting
    # divide by the number of tows in a year for standardised measure
    # This interpretation is not correct but it is true that the smaller the value
    # the more it is warm water selecting.
    #sum(cum.catch.E)-sum(cum.E)
    #curve.area= round(sum(cum.catch.E-cum.E),3)#/length(cum.E),3)
    #print(paste("difference in curve areas =",curve.area))

  # find the breakpoints of the cumulative catch
    # warning suppressed as they occur when a bp is not found
    # but the error handling is done in the bp function so this
    # prevents a bunch of output to the screen for an issue that
    # is already well handled within the function.
    suppressWarnings(
      {
      bps=c(-999,-999, 0, 0)
      bps <- bp.f(stn.E, cum.catch.E, percentile.search=percentile.search)
      Ezone.bp= diff(bps[1:2])
      cumcatch.at.bp= cum.catch.E[match(bps[1:2],stn.E)] # cumulative catch proportion at the breakpoints
      }
    )
    
  # find the E at the p1th and p2th percentiles of cumulative catch
    # the E at the exact percentile is calculated via interpolation (inside percentile function)
    # i.e. one station's catch may cumulate to 12% of the catch
    # while the next station cumulates to 40% of the catch and I want the 
    # E corresponding to the catch at the 25th percentile of cumulative catch
    # Therefore, it needs to interpolate E between 12% and 40%
    suppressWarnings(
      {
      E.percentiles <- Perry.percentiles(x=stn.E, y=cum.catch.E, P1=p1, P2=p2)
      cumcatch.at.p= c(p1,p2)
      Ezone.p= diff(E.percentiles)
      }
    )
    
    # percentile names
    name.p1= paste0("p1.",p1)
    name.p2= paste0("p2.",p2)

  # make plots  
    if(plots){
      def.par.perry <- par(no.readonly = TRUE)
      layout(matrix(c(1,3,3,2,3,3), 2, 3, byrow = TRUE), respect = TRUE)

      # catch vs E
        aggregated.E= aggregate(std.catch,list(floor(stn.E)),sum)
        aggregated.E[,1]= aggregated.E[,1]+0.5
        plot(aggregated.E, type="p",pch="-", cex=5, col="skyblue", xlab="E variable",
             ylab="Proportion of catch",
             ylim=c(0,max(aggregated.E[,2]*1.1)))
        points(stn.E, std.catch, pch=20)
        
      # Perry cumulative curves
        plot(stn.E, cum.catch.E, type="l", col="blue", xlab="E variable", 
             ylab="Cumulative proportion", ylim=c(0,1), lwd=2)
        lines(stn.E, cum.E,  lwd=2, col="grey")
        # vertical lines for breakpoints and E at max diff on the graph
        abline(v=bps[1:2], lty=2, col="green")
        abline(v=E.percentiles, lty=2, col="red")
        abline(v=max.diff.E, col="black", lty=1)
        ptext= paste0("percentile ", name.p1,", ",name.p2)
        bptext= paste0("breakpoints, ",percentile.search)
        legend("topleft", lty=c(1,1,2,2,1), col=c("grey","blue","green","red","black"),bty="n",cex=0.7,
               legend=c("cumulative E","cumulative catch",bptext,ptext,"max difference"))
        
      # a map
        # need to convert coordinates to decimal degrees and make them event data
          x= data$longitude
          y= data$latitude
          x= -decdeg(x)
          y= decdeg(y)
          z= data$biomass
          C= data$temperature
          catches= data.frame(EID=1:length(x), X=x, Y=y, Z=z, C=C)
          fishing.events= as.EventData(catches)
          map.f(longs=c(-71,-55), lats=c(45,52))
          alpha= 0.6 # transparency of catch bubbles. 0 = fully transparent
          #points(x, y, col=C, pch=20)
          if(E.field){ map.E.color(subdata); alpha <- 0}
          addBubbles(fishing.events,type="perceptual", symbol.bg=rgb(.9,.5,0,alpha=alpha),
                     legend.type="nested", symbol.zero="+",
                     legend.title="Biomass")

          
      # title the plot panel with species and year information
        yrinfo <- unique(data$year); specinfo <- unique(data$species)
        title(paste("Year =",yrinfo,";","Species =",specinfo))
      par(def.par.perry)
    }
  E.breaks= c(bps[1:2], E.percentiles, Ezone.bp, Ezone.p, max.diff.E, 
              cumcatch.at.bp, cumcatch.at.p, bps[3:4])
  names(E.breaks)= c("bp1", "bp2", name.p1, name.p2, "Ezone.bp", "Ezone.p","max.diff.E",
                     "cumcatch.at.bp1", "cumcatch.at.bp2", 
                     "cumcatch.at.p1","cumcatch.at.p2", "bp1.flag","bp2.flag")
  E.breaks
}

