# Perry curve functions and data filtering
#################
# TO DO
#################
# create a simulation function that is sparse with very few catches
# for simulation, randomly sample them from different distributions rather than polynomials
# uniform, normal, lognormal, pert, ,multinomial, negative binomial
# assign stations to depth and allow a selection by depth
# for simulations, pick a subsample of station locations and assign catches to them

# determine the effective area towed by a concave hull around survey points (remove Anticosti)
# calculate mean biomass per tow by species and year. Do occupancy abundance. The Ezone likely
# has density dependent effects where the fish will spread into suboptimal habitats if density 
# is high in the optimal ones

# 1. Fit a gompertz curve to the cumulated catch (by increasing E). 
# 2. Derive a new Zseries from the fit at equally spaced E.
# 3. Compute the numerical derivative by differencing.
# 4. Find the two points of maximum curvature
# 5. replace 3-5 by the analytical derivative
# 6. also determine if there is a positive y intercept. Perhaps constrain it to find
# the minimum values of the curves between [0, 0.25] and [0.75, 1].
# finally, I just did point 6 after trying lots of different stuff


#################
# breakpoint function 
#################
#' Find Breakpoints in Percentiles of a Relationship 
#'
#' This function searches for two breakpoints in the relationship between two variables. 
#' The breakpoints are located at specified percentiles of the dependent variable ('y'). 
#' This is symmetrical so if 0.25, it will look for a breakpoint in the first 25 percent of
#' cumulated catch at increasing E values and the last 25 percent of catch values 
#' cumulated by increasing E values. It incorporates error handling for cases where there is 
#' insufficient data.
#'
#' @param x A numeric vector representing the independent variable.
#' @param y A numeric vector representing the dependent variable.
#' @param percentile.search A numeric value between 0 and 0.5 (default: 0.25) specifying the  percentiles for breakpoint search.  
#'
#' @return A numeric vector of length 2 containing the x-coordinates of the identified breakpoints. 
#' If insufficient data exists for breakpoint analysis in a given interval, the lowest (or highest) 'x' value within that interval is used.
#' A flag is given for each breakpoint to show if this was done or not.
#' @export
bp.f = function(x, y, percentile.search = 0.25) {
  # search the 0 to percentile.search for breakpoint1, 1-percentile.search to 1 for the second breakpoint.
  # depends on the library strucchange

  # find the breakpoint in the first interval
  low.interval <- y <= percentile.search 
  x1 <- x[low.interval]
  y1 <- y[low.interval]

  # Error Handling for Breakpoint 1
  bp1 <- tryCatch(
    {
      bp <- breakpoints(y1 ~ x1, breaks = 1) 
      bp.x <- bp$breakpoints
      # sometimes the function will return NA instead of an error. just choose min x if this occurs
      if (is.na(bp.x)) bp.x <- 1
      x1[bp.x]  # Return the value directly
    },
    error = function(err) {
      # If error, use the lowest x value
      warning("Insufficient data for breakpoint analysis in the lower interval. Using lowest x value.") 
      min(x) #x and not x1 because sometimes x1 has no data
    }
  )

  # find the breakpoint in the second interval
  high.interval <- y > (1 - percentile.search)
  x2 <- x[high.interval]
  y2 <- y[high.interval]

  # Error Handling for Breakpoint 2
  bp2 <- tryCatch(
    {
      bp <- breakpoints(y2 ~ x2, breaks = 1) 
      bp.x <- bp$breakpoints
      # sometimes the function will return NA instead of an error. just choose min x if this occurs
      if (is.na(bp.x)) bp.x <- length(x2)
      x2[bp.x]  # Return the value directly
    },
    error = function(err) {
      # If error, use the highest x value
      warning("Insufficient data for breakpoint analysis in the upper interval. Using highest x value.")
      max(x) #x and not x2 because sometimes x2 has no data. Actually, I am not sure that can happen but this code is more robust anyway.
    }
  )
  if(bp1==min(x)) bp1.flag=0 else bp1.flag=1 # a flag to show if the breakpoint was estimated or if it just the lowest or highest value. 1 = estimated, 0 = min/max
  if(bp2==max(x)) bp2.flag=0 else bp2.flag=1

  bps <- c(bp1, bp2, bp1.flag, bp2.flag)
  bps
}


# old breakpoint function that finds points in the middle sometimes
# #' Find Breakpoints in a Relationship 
# #'
# #' This function uses the `breakpoints` function from the 'strucchange' library 
# #' to find breakpoints (points of structural change) in the  relationship between two variables.
# #'
# #' @param x Numeric vector representing the independent variable.
# #' @param y Numeric vector representing the dependent variable.
# #'
# #' @return A numeric vector of length 2 containing the x-coordinates of the detected breakpoints. 
# #' If only one breakpoint is found, the second element will be NA. If no breakpoints are found, both elements will be NA.
# #'
# #' @export
# #'
# #' @examples
# #' x <- 1:100
# #' y <- c(rnorm(50), rnorm(50, mean = 5))  # Simulate data with a break
# #' bp.f(x, y)
# bp.f = function(x, y){
#   # Depends on the library strucchange
#   bp = breakpoints(y ~ x, breaks=2) 
#   bp.x = bp$breakpoints
#   bps = x[bp.x]
# 
#   if (length(bps) == 1) bps = c(bps, NA) # if only one breakpoint is found, create an NA for the second one
#   if (length(bps) > 2) bps = c(NA, NA)  # Handle potential for more than 2 breaks 
# 
#   bps
# }


#################
# calculate the E at percentiles. This is moved into a function so I can use tryCatch for error detection
#################
#' Calculate Environmental Percentiles Based on Catch Distribution
#'
#' This function calculates specific percentiles of an environmental variable
#' corresponding to cumulative catch distribution percentiles. This is likely 
#' useful for fisheries or ecological analyses.
#'
#' @param x A numeric vector representing the environmental variable.
#' @param y A numeric vector representing the cumulative catch.
#' @param P1 Numeric value between 0 and 1 (default: 0.05) specifying the lower
#'           percentile of the cumulative catch.
#' @param P2 Numeric value between 0 and 1 (default: 0.95) specifying the upper
#'           percentile of the cumulative catch.
#'
#' @return A numeric vector of length 2 containing the environmental variable values 
#'         at the specified percentiles of the cumulative catch distribution. The E values
#'         are determined at the exact percentiles via interpolation. i.e. one station's 
#'         catch may cumulate to 12% of the catch while the next station cumulates to 40% 
#'         of the catch and I want the E corresponding to the catch at the 25th percentile 
#'         of cumulative catch. Therefore, it needs to interpolate E between 12% and 40%.
#'         If the cumulative catch at the first station (lowest E) is already more than p1
#'         of the total catch, the function will choose the lowest temperature as p1.
#' @export
Perry.percentiles= function(x, y, P1=0.05, P2=0.95){
  # y is cumulative catch and x in the environmental variable. Make y
  # the independent variable so I can interpolate catch to exact percentile
  E.range= range(x)
  E.percentiles= approx(y,x, xout=c(P1,P2))$y
  E.percentiles[is.na(E.percentiles)] <- E.range[1]
  E.percentiles
}


#################
# find out names of species associated with codes
#################
#' Find Species Information Based on Code or Name
#'
#' This function searches a species database and retrieves information based on 
#' a provided search term, which can be either a numeric species code or a 
#' partial species name (in English, French, or Latin).
#'
#' @param search.term A numeric species code or a partial species name (character string).
#'
#' @return A data frame containing:
#'   * **Code QC:** Species code used in Quebec surveys.
#'   * **English:** Common English name.
#'   * **French:** Common French name.
#'   * **Latin:** Scientific Latin name.
#' 
#' @export
#'
#' @examples
#' # Example 1: Search by numeric code 
#' find.species.f(8111) 
#'
#' # Example 2: Search by partial name (case-insensitive)
#' find.species.f("cod")
find.species.f = function(search.term){

  # join species table with data table so codes are not looked up when there are no records for them
  species.with.data = unique(biomass.w.zeros$species)
  species.table2 = species.table[species.table$codeqc %in% species.with.data,]

  if(is.numeric(search.term)){
    ## Search by numeric code
    vars = species.table2[like(species.table2$codeqc, search.term), c(7, 9:11)] 
  }

  if(is.character(search.term)){
    ## Search by partial name (in any of the name fields)
    vars1 = species.table2[like(species.table2$English, search.term, ignore.case=T), c(7, 9:11)]
    vars2 = species.table2[like(species.table2$French, search.term, ignore.case=T), c(7, 9:11)]
    vars3 = species.table2[like(species.table2$Latin, search.term, ignore.case=T), c(7, 9:11)]

    vars = rbind(vars1, vars2, vars3)

    ## Remove duplicates in case a name appears in multiple fields
    vars <- vars[!duplicated(vars), ]
  }

  vars # Return the results
}


#################
# convert survey lats and longs to decimal degrees
#################
#' Convert Survey Coordinates to Decimal Degrees
#'
#' This function converts survey coordinates in the format of degrees, minutes, and decimal minutes (e.g., 4838.73) into decimal degrees.
#'
#' @param degmin.sec Numeric value representing the coordinates in degrees, minutes, and decimal minutes format.
#' @return Numeric value representing the coordinates in decimal degrees.
#' @export
#'
#' @examples
#' decdeg(4438.73) # Returns 44.6455
decdeg <- function(degmin.sec) {
  # Extract degrees and minutes
  deg <- floor(degmin.sec / 100)
  min <- floor(degmin.sec %% 100) # Use modulo for minutes

  # Extract decimal minutes
  decimal_minutes <- degmin.sec - deg * 100 - min

  # Convert minutes and decimal minutes to decimal form
  dd <- deg + min/60 + decimal_minutes/60

  return(round(dd, 5)) # Round for accuracy
}

#decdeg(4438.73)

#################
# Rescale a vector to new range of values 
#################
#' Rescales a numeric vector 
#'
#' This function rescales a numeric vector to a specified new range, 
#' preserving the relative differences between the original values.
#' 
#' @param x A numeric vector to be rescaled.
#' @param new_min The desired minimum value for the rescaled vector (default is 0).
#' @param new_max The desired maximum value for the rescaled vector (default is 1).
#'
#' @return A numeric vector, rescaled to the specified range.
#'
#' @examples
#' original_data <- c(-5, 2, 8, 10)
#' rescaled_data <- rescale(original_data, new_min = 0, new_max = 10)
#' rescaled_data 
#' # [1]  0.0  3.33  8.89 10.00 
rescale <- function(x, new_min = 0, new_max = 1) {
  # Calculate the minimum and maximum values in the input vector
  old_min <- min(x)
  old_max <- max(x)

  # Rescale the data
  rescaled= (x - old_min) / (old_max - old_min) * (new_max - new_min) + new_min

  # Return the rescaled vector
  rescaled
}

#################
# Rescale a vector to new range of values 
#################
#' Unscales a numeric vector 
#'
#' This function reverses the rescaling performed by the 'rescale' function, 
#' mapping values from a previously rescaled range back to the original range.
#' Effectively the same function as the rescale but just makes it the unscaling
#' more explicitly stated.
#'
#' @param x A numeric vector to be unscaled.
#' @param old_min The original minimum value of 'x' before rescaling.
#' @param old_max The original maximum value of 'x' before rescaling.
#' @param new_min The minimum value of the rescaled range of 'x' (default is 0).
#' @param new_max The maximum value of the rescaled range of 'x' (default is 1).
#'
#' @return A numeric vector, unscaled back to the original range.
#'
#' @examples
#' original_data <- c(-5, 2, 8, 10)
#' rescaled_data <- rescale(original_data) 
#' unscaled_data <- unscale(rescaled_data, min(original_data), max(original_data))
#' unscaled_data
#' # [1] -5  2  8 10
unscale <- function(x, old_min, old_max, new_min = 0, new_max = 1) {
  # Unscale the data
  unscaled= (x - new_min) / (new_max - new_min) * (old_max - old_min) + old_min

  # Return the unscaled vector
  unscaled
}