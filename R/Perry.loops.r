################
# The function to loop over years and species combinations
################
#' Perform analysis of biomass data using the Perry.f function in a loop over years and species
#' 
#' This function iterates over species and years in a dataset, applies the `Perry.f` function to subsets of data, handles empty data scenarios, and organizes results into a structured data frame. 
#'
#' @param data A data frame containing at least the following columns: 'species', 'year', and other variables required by the `Perry.f` function.
#' @param percentile.search A numeric value between 0 and 0.5 (default: 0.25) specifying the  percentiles for breakpoint search. See bp.f
#' @param p1 A numeric value (likely a probability) used within the `Perry.f` function. Default is 0.05.
#' @param p2 A numeric value (likely a probability) used within the `Perry.f` function. Default is 0.95.
#'
#' @return A data frame with the following columns:
#'   * **species:** Species identifier
#'   * **year:** Year
#'   * **bp1, bp2, p1, p2:** Results from the `Perry.f` function (their meaning depends on the implementation of `Perry.f`) 
#'   * **Ezone.bp, Ezone.p, max.diff.E, cumcatch.at.bp1, bp1.flag, bp2.flag:** Additional results (dependent on `Perry.f`)
#' @examples
#' subdata= biomass.w.zeros[year %in% 1997:2003 & species %in% 438] # cod 1997 to 2003
#' Perry.loop.f(subdata) 
#' @export
Perry.loop.f = function(data, percentile.search=0.25, p1 = 0.05, p2 = 0.95,
                        T.rescaling=2){
  years <- unique(data$year)
  speciess <- unique(data$species)

  # Initialize results matrix
  breaks <- matrix(NA, ncol = 15)

  # Iterate over species
  for (i in speciess){
    print(paste("species", i)) 

    # Iterate over years
    for (ii in years){
      print(ii) 

      # Create data subset for current species-year combination
      subdata <- data[species == i & year == ii]

      # If data exists, apply Perry.f and store results
      if(nrow(subdata) > 0){  
        breaksi.ii <- c(i, ii, 
        Perry.f(subdata, percentile.search=percentile.search, 
          p1 = p1, p2 = p2, T.rescaling = T.rescaling))
        breaks <- rbind(breaks, breaksi.ii) 
      } else { 
        # If no data, store placeholder row with error codes
        breaksi.ii <- c(i, ii, rep(-666, 7)) 
        breaks <- rbind(breaks, breaksi.ii)  
      } 
    } 
  } 

  # Clean up results, format as data frame, and assign column names
  remove.rows <- apply(breaks[, 3:9], 1, sum, na.rm = TRUE) != -4662 
  remove.rows[1] <- FALSE 
  breaks <- as.data.frame(breaks[remove.rows, ])
  names(breaks) <- c("species", "year", "bp1", "bp2", "p1", "p2", "Ezone.bp", "Ezone.p",
                     "max.diff.E", "cumcatch.at.bp1", "cumcatch.at.bp2", 
                     "cumcatch.at.p1", "cumcatch.at.p2","bp1.flag","bp2.flag")
  row.names(breaks) <- NULL 
  breaks 
}


################
# The function to loop over years and species combinations, E weighted catch
################
#' Perform analysis of biomass data using the Perry.f function in a loop over years and species. 
#' The catch is E weighted like in Perry and Smith 1994
#' 
#' This function iterates over species and years in a dataset, applies the `Perry.f` function to subsets of data, handles empty data scenarios, and organizes results into a structured data frame. 
#'
#' @param data A data frame containing at least the following columns: 'species', 'year', and other variables required by the `Perry.f` function.
#' @param percentile.search A numeric value between 0 and 0.5 (default: 0.25) specifying the  percentiles for breakpoint search. See bp.f
#' @param p1 A numeric value (likely a probability) used within the `Perry.f` function. Default is 0.05.
#' @param p2 A numeric value (likely a probability) used within the `Perry.f` function. Default is 0.95.
#'
#' @return A data frame with the following columns:
#'   * **species:** Species identifier
#'   * **year:** Year
#'   * **bp1, bp2, p1, p2:** Results from the `Perry.f` function (their meaning depends on the implementation of `Perry.f`) 
#'   * **Ezone.bp, Ezone.p, max.diff.E, cumcatch.at.bp1, bp1.flag, bp2.flag:** Additional results (dependent on `Perry.f`)
#' @examples
#' subdata= biomass.w.zeros[year %in% 1997:2003 & species %in% 438] # cod 1997 to 2003
#' Perry.E.weighted.catch.loop.f(subdata) 
#' @export
Perry.E.weighted.catch.loop.f = function(data, percentile.search=0.25, 
                                  p1 = 0.05, p2 = 0.95, T.rescaling=2){
  years <- unique(data$year)
  speciess <- unique(data$species)

  # Initialize results matrix
  breaks <- matrix(NA, ncol = 15)

  # Iterate over species
  for (i in speciess){
    print(paste("species", i)) 

    # Iterate over years
    for (ii in years){
      print(ii) 

      # Create data subset for current species-year combination
      subdata <- data[species == i & year == ii]

      # If data exists, apply Perry.f and store results
      if(nrow(subdata) > 0){  
        breaksi.ii <- c(i, ii, 
        Perry.E.weighted.catch.f(subdata, percentile.search=percentile.search, 
          p1 = p1, p2 = p2, T.rescaling=T.rescaling))
        breaks <- rbind(breaks, breaksi.ii)
      } else { 
        # If no data, store placeholder row with error codes
        breaksi.ii <- c(i, ii, rep(-666, 7)) 
        breaks <- rbind(breaks, breaksi.ii)  
      } 
    } 
  } 

  # Clean up results, format as data frame, and assign column names
  remove.rows <- apply(breaks[, 3:9], 1, sum, na.rm = TRUE) != -4662 
  remove.rows[1] <- FALSE 
  breaks <- as.data.frame(breaks[remove.rows, ])
  names(breaks) <- c("species", "year", "bp1", "bp2", "p1", "p2", "Ezone.bp", "Ezone.p",
                     "max.diff.E", "cumcatch.at.bp1", "cumcatch.at.bp2", 
                     "cumcatch.at.p1", "cumcatch.at.p2","bp1.flag","bp2.flag")
  row.names(breaks) <- NULL 
  breaks 
}
