## What is it

A library to fit curves to survey data to find habitat preferences or
tolerance to environmental conditions. Based on Perry and Smith (1994).

## Finding a species by name or code

    find.species.f(438)

    ##    codeqc      English        French        Latin
    ##     <num>       <char>        <char>       <char>
    ## 1:    438 Atlantic cod morue franche Gadus morhua

    find.species.f("squid")

    ##    codeqc                    English                            French
    ##     <num>                     <char>                            <char>
    ## 1:   4569       lesser bobtail squid               sépiole calamarette
    ## 2:   4587    butterfly bobtail squid sépiole (Stoloteuthis leucoptera)
    ## 3:   4591         squid (Teuthoidea)               calmar (Teuthoidea)
    ## 4:   4595 inshore squid (Loligo sp.)            casserons (Loligo sp.)
    ## 5:   4753    northern shortfin squid           encornet rouge nordique
    ##                      Latin
    ##                     <char>
    ## 1:       Semirossia tenera
    ## 2: Stoloteuthis leucoptera
    ## 3:              Teuthoidea
    ## 4:              Loligo sp.
    ## 5:      Illex illecebrosus

## Selecting data

    # 2020 and cod (438)
    subdata= biomass.w.zeros[year %in% 2020 & species %in% 438]

    # other options
    #subdata= biomass.w.zeros[year %in% 2020:2022 & species %in% c(438,792,4753)]

## Construct the cumulatives curves, find breakpoints, plot

The percentile.search is 0.25 which means the first breakpoint is sought
within the cumulative biomass curve &lt;25% and the second breakpoint is
the biomass curve&gt;75% of the biomass.

    # uses the subdata selected in the previous code chunk
    Perry.f(subdata,
      percentile.search = 0.25,
      p1 = 0.05,
      p2 = 0.95,
      T.rescaling = 2,
      plots = TRUE,
      E.field = FALSE)

![](README_files/figure-markdown_strict/simple-1.png)

    ##             bp1             bp2         p1.0.05         p2.0.95        Ezone.bp 
    ##    -0.780000000     5.790000000    -0.389476708     5.219761039     6.570000000 
    ##         Ezone.p      max.diff.E cumcatch.at.bp1 cumcatch.at.bp2  cumcatch.at.p1 
    ##     5.609237747     5.230000000     0.007804359     0.967012561     0.050000000 
    ##  cumcatch.at.p2        bp1.flag        bp2.flag 
    ##     0.950000000     0.000000000     1.000000000

One specifies the percentile search and percentiles. The
percentile.search if = 0.25 will try to estimate a two line segmented
regression in the first 5% of the cumulated catch values and will
likewise do the same in the last 5% of the cumulated catch values. If
the percentiles are 0.25 and 0.75, then this is like a hierarchical or a
refining approach where the percentiles are used to estimate the
quartiles and interquartile range (p2-p1) while the breakpoint method
searches for finer structure within the first and last quartile of catch
values.

Estimation flags show whether or not the breakpoints were estimated (1)
or taken as the min or max of the temperature series (0).

The basic Perry.f function does not look at years or species. Therefore,
if you select multiple years and or multiple species, it will just
construct one set of curves lumped over all of them. This may be
desirable in some special cases.

### Annotated basic graph and output

![](./README_files/figure-markdown_strict/graph.description.png)

The annotated graphs shows the meaning of various points and lines in
the main graphical output of the package.

The output of the Perry functions consists mostly of a series of
breakpoints and quantities of interest from the cumulative distribution
graphs

![](./README_files/figure-markdown_strict/perry.output.description.png)

## Loop over years and species

A loop function has been developed to run the analysis over multiple
species and years and it save the output to a data.frame. It does not
make any plots.

    find.species.f(8111) # shrimp

    ##    codeqc         English            French             Latin
    ##     <num>          <char>            <char>            <char>
    ## 1:   8111 northern shrimp crevette nordique Pandalus borealis

    find.species.f(193) # argentine

    ##    codeqc            English           French           Latin
    ##     <num>             <char>           <char>          <char>
    ## 1:    193 Atlantic argentine grande argentine Argentina silus

    subdata= biomass.w.zeros[year %in% 2015:2022 & species %in% c(8111,193)]
    Perry.loop.f(subdata, percentile.search=0.25, p1=0.05, p2=0.95, 
                 T.rescaling = 2)

    ## [1] "species 8111"
    ## [1] 2015
    ## [1] 2016
    ## [1] 2017
    ## [1] 2018
    ## [1] 2019
    ## [1] 2020
    ## [1] 2021
    ## [1] 2022
    ## [1] "species 193"
    ## [1] 2015

    ## Warning in rbind(breaks, breaksi.ii): number of columns of result is not a
    ## multiple of vector length (arg 2)

    ## [1] 2016
    ## [1] 2017
    ## [1] 2018
    ## [1] 2019
    ## [1] 2020
    ## [1] 2021
    ## [1] 2022

    ##    species year   bp1   bp2       p1       p2 Ezone.bp   Ezone.p max.diff.E
    ## 1     8111 2015 5.440 6.440 4.184808 6.430000    1.000 2.2451924      5.730
    ## 2     8111 2016 3.810 6.420 3.185648 6.365191    2.610 3.1795438      5.720
    ## 3     8111 2017 4.280 6.340 2.322222 6.487616    2.060 4.1653939      5.600
    ## 4     8111 2018 2.130 6.740 2.095379 6.740000    4.610 4.6446207      5.740
    ## 5     8111 2019 1.727 6.422 1.712545 6.542735    4.695 4.8301900      5.929
    ## 6     8111 2020 1.890 6.480 1.610644 6.968970    4.590 5.3583266      5.800
    ## 7     8111 2021 1.950 6.730 1.363538 7.081526    4.780 5.7179887      2.980
    ## 8     8111 2022 1.200 6.970 1.208224 6.979554    5.770 5.7713305      2.630
    ## 9      193 2016 5.430 6.990 5.334305 6.763021    1.560 1.4287155      6.000
    ## 10     193 2017 5.910 6.630 5.622402 6.608547    0.720 0.9861445      6.420
    ## 11     193 2018 6.260 6.910 6.561379 6.884446    0.650 0.3230666      6.620
    ## 12     193 2019 6.142 7.235 5.837039 6.904117    1.093 1.0670786      6.429
    ## 13     193 2020 6.120 7.200 6.175552 7.119904    1.080 0.9443515      6.940
    ## 14     193 2021 6.530 7.240 6.496104 7.028819    0.710 0.5327146      6.620
    ## 15     193 2022 6.220 7.860 6.137198 7.738922    1.640 1.6017241      6.830
    ##    cumcatch.at.bp1 cumcatch.at.bp2 cumcatch.at.p1 cumcatch.at.p2 bp1.flag
    ## 1     0.0745424181       0.9748663           0.05           0.95        1
    ## 2     0.0574242091       0.9575179           0.05           0.95        1
    ## 3     0.1177931020       0.9128891           0.05           0.95        1
    ## 4     0.0119816091       0.8807732           0.05           0.95        1
    ## 5     0.0044765060       0.8685044           0.05           0.95        1
    ## 6     0.0638586732       0.7632058           0.05           0.95        1
    ## 7     0.0538886961       0.7680273           0.05           0.95        1
    ## 8     0.0006364332       0.8895946           0.05           0.95        1
    ## 9     0.0065590429       1.0000000           0.05           0.95        1
    ## 10    0.0375256779       1.0000000           0.05           0.95        1
    ## 11    0.0323505657       1.0000000           0.05           0.95        1
    ## 12    0.0239876443       1.0000000           0.05           0.95        1
    ## 13    0.0164695664       1.0000000           0.05           0.95        1
    ## 14    0.0409711499       1.0000000           0.05           0.95        1
    ## 15    0.0237979925       1.0000000           0.05           0.95        1
    ##    bp2.flag
    ## 1         1
    ## 2         1
    ## 3         1
    ## 4         1
    ## 5         1
    ## 6         1
    ## 7         1
    ## 8         1
    ## 9         0
    ## 10        0
    ## 11        0
    ## 12        0
    ## 13        0
    ## 14        0
    ## 15        0

## Show a map of survey sets since 2009

    subdata <- biomass.w.zeros[year %in% 2009:2023]
    subdata <- subdata[,7:8]
    subdata <- unique(subdata) # unique station stations by year
    map.f()
    stat.area.f("ngsl.trawl.survey.strata")
    points(-decdeg(subdata$longitude),decdeg(subdata$latitude),
           pch=20,col="blue",cex=0.1)

![](README_files/figure-markdown_strict/setmap-1.png)

The strata for the Northern Gulf survey are also shown. The survey is
stratified random but we have treated it in this analysis as a random
survey.

## Compare E weighted catch vs unweighted

The original Perry and Smith (1994) method weights the catch at a
location by the temperature or habitat variable measured at that
location and then the catch is cumulated in order of increasing habitat
variable. This is a good method to find a temperature preference,
i.e. the temperature where the most catch of a species can be found.
However, if we are less interested in single temperature optimum but the
range of temperature which seem to be good for a species, it makes less
sense to weight the catch by the temperature. We have provided methods
for both.

    # Using cod in 1997 as an example
    subdata <- biomass.w.zeros[year %in% 1997 & species %in% 438]
    Perry.E.weighted.catch.f(subdata, percentile.search=0.25, T.rescaling=2, plot=T) 

![](README_files/figure-markdown_strict/weighted-1.png)

    ##                        bp1                        bp2 
    ##                -0.20000000                 5.70000000 
    ##                    p1.0.05                    p2.0.95 
    ##                -0.15339752                 6.10000000 
    ##                   Ezone.bp                    Ezone.p 
    ##                 5.90000000                 6.25339752 
    ##                 max.diff.E cumcatch.E.weighted.at.bp1 
    ##                 4.10000000                 0.02463672 
    ## cumcatch.E.weighted.at.bp2  cumcatch.E.weighted.at.p1 
    ##                 0.85603504                 0.05000000 
    ##  cumcatch.E.weighted.at.p2                   bp1.flag 
    ##                 0.95000000                 1.00000000 
    ##                   bp2.flag 
    ##                 1.00000000

Then the unweighted catch where the catch is simply cumulated in order
of increasing temperature

    Perry.f(subdata, percentile.search=0.25, T.rescaling=2, plot=T) 

![](README_files/figure-markdown_strict/unweighted-1.png)

    ##             bp1             bp2         p1.0.05         p2.0.95        Ezone.bp 
    ##     -1.10000000      4.80000000     -0.85412050      5.90000000      5.90000000 
    ##         Ezone.p      max.diff.E cumcatch.at.bp1 cumcatch.at.bp2  cumcatch.at.p1 
    ##      6.75412050      4.10000000      0.03303408      0.90669147      0.05000000 
    ##  cumcatch.at.p2        bp1.flag        bp2.flag 
    ##      0.95000000      0.00000000      1.00000000

This is bit of extreme scenario in that cod was found in cold water as
well as warm water in 1997 but when weighting the catch by temperatures
at or near zero, the weighted catch method practically eliminates those
catches and therefore it makes cod seem like it has a narrower
temperature tolerance than it actually does.

## How to deal with negative temperatures

The Perry and Smith methods depend on making cumulative curves of
temperature or an environmental variable and of catch at stations. Catch
is always 0 or greater but temperatures can be negative. This is problem
for cumulating curves as it will lead to a decreasing part of the curve.

We have developed three and a sort of hidden fourth options for dealing
with negative temperatures:

1.  Before doing any analysis, make all temperatures &lt; 0 equal to 0.
2.  Shift the temperature series to the right by the smallest value in
    the temperature series if there is a value &lt; 0. If there are no
    negative values then do nothing.
3.  Truncate all the data (catches and temperature) where temperature
    values are &lt; 0.
4.  Convert temperature to energy (kJ per g water) and plot this instead
    as energy content is an actual scale with minimum of zero unlike
    temperature. This is commented out in the code because it makes the
    same curve as method 2 but with a different E scale and the
    histogram on the top left would need to be adjusted. One could also
    just express temperature in Kelvin.

### Implications of negative temperature methods

1.  This is the default option. In reality there are usually not that
    many stations with negative temperature (say 2-5%). By making all
    negative temperatures=0, it will push all the catches at negative
    temperatures to be assigned to zero. There is rarely much catch of
    anything &lt; 0 so this is not usually a problem. Ecologically
    speaking, the difference between 0 and -1 is probably not that
    different so it is not a huge error to attribute those catches to 0.
    Finally, most of the negative bottom water temperatures in the Gulf
    are found in the Mecatina trough and the survey cannot really fish
    in this area owing to the bottom type which rips up the gear,
    i.e. we rarely fish in negative temperatures.

2.  Shifting the curve to the right when necessary is also a good option
    and few implications. It has the positive aspect of only shifting
    the curve for the cumulation aspect but breakpoints, percentiles and
    the graph x axis are still calculated and shown with the original
    temperature scale. It will have the impact of spreading out those
    catches at negative temperature along the full observed temperature
    scale.

3.  Truncating all data where temperature &lt; 0 is also a good option
    as it rarely would eliminate much data (2-5%) and those data it
    eliminates are usually very small catches that have little impact on
    the cumulated catch curve.

4.  This produces the same catch curve as method 2 for the unweighted
    method. It will impact the weighted method however.

## References

Perry, R.I., and S.J. Smith. 1994. Identifying habitat associations of
marine fishes using survey data: an application to the northwest
Atlantic. Can. J. Fish. Aquat. Sci. 51: 589-602.
